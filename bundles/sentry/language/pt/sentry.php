<?php
/**
 * Part of the Sentry bundle for Laravel.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Sentry
 * @version    1.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2012, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return array(

	/** General Exception Messages **/
	'account_not_activated'  => 'Usuário não ativou sua conta.',
	'account_is_disabled'    => 'Esta conta foi desativada.',
	'invalid_limit_attempts' => 'Sentry configuração do artigo: "limit.attempts" deve ser um número inteiro maior que 0',
	'invalid_limit_time'     => 'Sentry configuração do artigo: "limit.time" deve ser um número inteiro maior que 0',
	'login_column_empty'     => 'Você deve definir "login_column" na configuração Sentry.',

	/** Group Exception Messages **/
	'group_already_exists'      => 'O nome do grupo. ":group" já existe',
	'group_level_empty'         => 'Você deve especificar um nível do grupo.',
	'group_name_empty'          => 'Você deve especificar o nome do grupo. ',
	'group_not_found'           => 'Grupo ":group" não existe.',
	'invalid_group_id'          => 'Grupo ID deve ser um número inteiro maior que 0.',
	'not_found_in_group_object' => '":field" Não existe no objeto "grupo"',
	'no_group_selected'         => 'Nenhum grupo foi selecionado.',
	'user_already_in_group'     => 'O usuário já está no grupo ":group".',
	'user_not_in_group'         => 'O usuário não está no grupo ":group".',

	/** User Exception Messages **/
	'column_already_exists'           => ':column já existe.',
	'column_and_password_empty'       => ':column e Password não pode estar vazio.',
	'column_email_and_password_empty' => ':column, Email and Password não pode estar vazio.',
	'column_is_empty'                 => ':column não pode estar vazio.',
	'email_already_in_use'            => 'Esse e-mail já esta em uso.',
	'invalid_old_password'            => 'A senha antiga é inválida.',
	'invalid_user_id'                 => 'ID do usuário deve ser um número inteiro maior que 0.',
	'no_user_selected'                => 'Você deve primeiro selecionar um usuário.',
	'no_user_selected_to_delete'      => 'Nenhum usuário selecionado para excluir.',
	'no_user_selected_to_get'         => 'Nenhum usuário selecionado.',
	'not_found_in_user_object'        => '":field" não existe no objeto "user".',
	'password_empty'                  => 'A senha não pode esta vazia.',
	'user_already_enabled'            => 'O usuário já está habilitado.',
	'user_already_disabled'           => 'O usuário já está desativado.',
	'user_not_found'                  => 'O usuário não existe.',
	'username_already_in_use'         => 'Este nome de usuário já está em uso.',

	/** Attempts Exception Messages **/
    'login_ip_required'    => 'ID de login e endereço IP são obrigados a adicionar uma tentativa de login.',
    'single_user_required' => 'Tentativas só pode ser adicionada a um único utilizador, uma matriz foi determinada.',
    'user_suspended'       => 'Você foi suspenso por tentar entrar na conta ":account" por :time minutos.',

    /** Hashing **/
    'hash_strategy_null'      => 'Estratégia Hashing é nulo ou vazio. A estratégia de hashing deve ser definido.',
    'hash_strategy_not_exist' => 'Estratégia Hashing arquivo não existe.',

	/** Permissions Messages **/
	'no_rules_added'    => 'Oops, você esqueceu de especificar quaisquer regras a serem adicionadas.',
	'rule_not_found'    => 'A regra :rule, não existe em suas regras configuradas. Por favor verifique as regras na configuração sentry.',
	'permission_denied' => 'Oops, você não tem permissão para acessar :resource',

);
