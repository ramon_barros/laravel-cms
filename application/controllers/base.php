<?php

class Base_Controller extends Core_Controller {
	
	public function __construct(){
		parent::__construct();

		/* 
		  Método de exemplo que será executado em todos os controllers
		*/
		$this->functionGlobal();

		/*
		  Método de exemplo que será executado somente no controller teste e em todos os seus métodos
		 */
		if($this->validateController('teste')){
			$this->functionTeste();
		}

		/*
		 Método de exemplo que será executado somente no controller bar e método teste
		 */
		if($this->validateController('bar') and $this->validateMethod('teste')){
			$this->functionTesteFooBarTeste();
		}
	}

	public function functionGlobal(){
		echo "<br/>";
		var_dump( array('controller' =>  $this->controller,'method' => $this->method ,'functionGlobal' => 'Esta função é executada em todas as controllers') );
		echo "<br/>";
	}

	public function functionTeste(){
		echo "<br/>";
		var_dump( array('controller' =>  $this->controller,'method' => $this->method ,'functionTeste' => 'Esta função é executada somente no controller Teste') );
		echo "<br/>";
	}

	public function functionTesteFooBarTeste(){
		echo "<br/>";
		var_dump( array('controller' =>  $this->controller,'method' => $this->method ,'functionTesteFooBarTeste' => 'Esta função é executada somente no controller bar e método get_teste() ') );
		echo "<br/>";
	}
}