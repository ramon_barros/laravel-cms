<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="<?= Config::get('application.language') ?>" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="<?= Config::get('application.language') ?>" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="<?= Config::get('application.language') ?>" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="<?= Config::get('application.language') ?>" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="author" href="humans.txt" />
    <meta name="keywords" content="">
    <title><?=$title?></title>
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?= asset('img/ico/favicon.ico');?>">
    <link rel="stylesheet" href="<?= asset('css/main.css')?>" />
    <?= Asset::styles() ?>
    <?php if ($_SERVER['HTTP_HOST']=='localhost') :?>
        <!--script src="<?=asset('libs/css/refresh.js')?>"> </script--> <!-- script que recarrega todos os css a cada 600ms -->
    <? endif;?>
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="container">
            <?= isset($header)?$header:'' ?>
            <?= isset($content)?$content:'' ?>
            <?= isset($footer)?$footer:'' ?>
        </div>
        <script type="text/javascript">
            var
                baseUrl            = '<?=URL::base()?>/',
                assetsUrl          = '<?=asset('')?>',
                baseImgModule      = '<?=asset('img/'.$uri[0].'/')?>',
                baseCssModule      = '<?=asset('css/'.$uri[0].'/')?>',
                baseJsModule       = '<?=asset('js/'.$uri[0].'/')?>',
                lang               = '<?=Config::get('application.language')?>',
                segments           = ('<?=URI::current()?>').split('/');
        </script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"> </script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!--script type="text/javascript" src="<?= asset('libs/infiniteCarousel.js')?>"></script-->
        <?= Asset::scripts() ?>
        <script type="text/javascript" src="<?= asset('js/main.js')?>"></script>
    </body>
</html>
