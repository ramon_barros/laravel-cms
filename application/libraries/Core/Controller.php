<?php namespace Core;

/////////////////////////////////////////////////////////////////////////
// Não alterar esta classe sem aprovação, pois irá influenciar em todo //
// projeto.                                                            //
/////////////////////////////////////////////////////////////////////////

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Cache;
//use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \FilesystemIterator;

/**
 * Métodos utilizado em todos os controllers
 */
class Controller extends \Laravel\Routing\Controller {

  /**
   * Indica se o controller Indicates if the controller uses RESTful routing.
   *
   * @var bool
   */
  public $restful = true;
  
  /**
   * Indica o layout padrão da view
   */
  public $layout = 'layouts.common';
  
  public $controller;

  public $method;

  public $data = array();
  
  private $uri = '';
  
  /**
   * Catch-all method for requests that can't be matched.
   *
   * @param  string    $method
   * @param  array     $parameters
   * @return Response
   */
  public function __call($method, $parameters)
  {
    return Response::error('404');
  }

  /**
   * Construtor da classe
   * Inicializa configurações
   */
  public function __construct(){
    parent:: __construct();
    /**
     * Retorna segimento da URL
     * @var string
     */
    $this->uri = explode('/', (strlen(URI::current())==1) ? 'home' :  URI::current());

    /**
     * Efetua o carregamento dos assets
     */
    $this->load_assets();

    /**
     * Inicializa macros e variavies que facilitam a produção.
     */
    $this->init();
  }

  /**
   * Carrega os js e css do website
   * @return void
   */
  public function load_assets(){
          $this->load_assets_jscss('js');
          $this->load_assets_jscss('css');
  }
  
  /**
   * Inicializa macros
   * @return void
   */
  private function init()
  {
    $macros = new \Macros();
    $this->data = array(
           'uri' => $this->uri
    );
    $this->layout->title = '';
    $this->layout->uri = $this->uri;
    $this
          ->parts('header', 'common.header')
          ->parts('footer', 'common.footer');
  }

  /**
   * Altera o layout principal da página em execução.
   * @param string $layout layout principal 
   */
  public function setLayout($layout='layouts.common'){
    View::name($layout, 'layout');
    $this->layout = View::of('layout');
  }

  /**
   * Adiciona conteudo nas variáveis no layout principal
   * @param  boolean $name 
   * @param  boolean $path 
   * @return $this
   */
  public function parts($name = false, $path = false){
    if($name and $path){
      $this->layout->{$name} = $this->view($path);
      return $this;
    }else{
      throw new Exception("Você deve passar o nome da variável contida no layout principal [$this->layout] e view atribuida a mesma.");
    }
  }

  /**
   * Simplifica utilização do View::make
   * @param  string $view view a ser carregada
   * @return string 
   */
  public function view($view){
    return View::make($view)->with($this->data);
  }

  /**
   * Recupera o nome do controller acessado
   * @return string
   */
  public function get_controller(){
    $controller = strtolower(preg_replace('/_Controller/','',get_class($this)));
    return $controller;
  }

  public function validateController($controller){
    return ($this->controller == $controller)?true:false;
  }

  public function validateMethod($method){
    return ($this->method == $method)?true:false;
  }

  /**
   * Efetua o carregamento dos assets
   * @param  string $type      tipo de arquivo
   * @param  string $directory diretoria dos arquivos
   * @return void
   */
  public function load_assets_jscss($type='js',$directory=null){
    /**
     * Se o diretório é nulo então utiliza o padrão.
     */
    if(is_null($directory))
    {
      // http://localhost/projeto/public/js/
      // http://localhost/projeto/public/css/
      $directory = path('public').$type.DS;
    }

    if(is_null($this->uri)){
            $this->uri = explode('/', (strlen(URI::current())==1) ? 'home' :  URI::current());
    }
    $controllers = explode('_', $this->get_controller());
    $controller = end($controllers);
    $method = current(array_diff_assoc($this->uri,$controllers));
    $method = $method?$method:'index';
    $path = implode(DS,$controllers);
    $url = URL::base();
    $directory_assets = $directory.$path;
    if(!is_dir($directory_assets)){
      $controllers = $this->uri;
      $controller = current($this->uri);
      
      $directory_assets = $directory.$controller;
      $method = current(array_diff_assoc($this->uri,$controllers));
      $method = $method?$method:'index';
    }

    $this->controller = $controller;
    $this->method = $method;
    
    if(is_dir($directory_assets)){
      $items = new FilesystemIterator($directory_assets, FilesystemIterator::SKIP_DOTS);
      foreach ($items as $item)
      {
            if( preg_match('/('.$controller.'|'.$method.')/', $item->getBasename()) )
            {
                    $file = $item->getBasename();
                    $filename = preg_replace('/([^.]*)(\.js|\.css)/', '$1', $file);
                    $ext = File::extension($file);
                    if($ext=='js' or $ext=='css'){
                            $path = str_replace('\\','/',str_replace(path('public'),'',$directory_assets));
                            Asset::add(str_replace('.', '-', $file),$url.'/'.$path.'/'.$file);
                    }
            }
      }
    }else{
      $this->log('error','Diretório não existe load_assets():'.$directory_assets);
    }
  }
}