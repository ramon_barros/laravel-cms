<?php

use \Laravel\HTML as HTML;

/**
 * Classe para definição de macros.
 */
class Macros {

        public function __construct(){
                HTML::macro('makeMenu', function($itens = '', $param = false)
                {
                        $html = '';
                        foreach ($param as $key => $value) {
                                $html .=  $key.'="'.$value.'"';
                        }
                        $html  = '<ul '.$html.' >';
                        foreach ($itens as $key => $item) {
                                $html .= '<li>'.HTML::link($item, $key).'</li>';
                        }
                        $html .= '</ul>';
                        return $html;
                });

                HTML::macro('imageLink', function($url = '', $img='img/', $alt='', $param = false, $active=true, $ssl=false)
                {
                        $url = $ssl==true ? URL::to_secure($url) : URL::to($url);
                        $img = HTML::image($img,$alt);
                        $link = $active==true ? HTML::link($url, '#', $param) : $img;
                        $link = str_replace('#',$img,$link);
                        return $link;
                });


        }
}