$(function(){

    $('.cancel').click(function(){
        $('#modal').modal('hide')
    });
    $('.delete').click(function(){
        event.preventDefault();
        console.info($(this).data('value'));
        $('#formDelete input:first')
            .val($(this).data('value'))
            .attr('id' , $(this).data('key'))
            .attr('name' , $(this).data('key'));
    });

    $( ".sortable" ).sortable();

     $( ".sortable" ).sortable({
      update: function(event, ui) {
        var order = new Array();
        $('.sortable li').each(function(index,elem) {
          order[index] = $(elem).attr('rel');
        });
        $.ajax({
          type: 'POST',
          url: url("admin/modulos/fotos/update_order"),
          data: 'data='+JSON.stringify(order),
        });
      }
    });
    $( ".sortable" ).disableSelection();

    $('.delete_toggler').each(function(index,elem) {
          $(elem).click(function(){
            $('#postvalue').attr('value',$(elem).attr('rel'));
            $('#delete_image').modal('show');
          });
      });

    $('#messages .table tbody tr').hover(function(){
        //var position = $(this).position();
        //$('#message-text-' + $(this).data('id'))
        $('.message-text').hide();
        $('#message-text-' + $(this).data('id')).css('marginTop',$(this).position().top-210).show();
    });
    $('#messages').mouseleave(function(){
        $('.message-text').hide();
    });

    // admin/mensagens/update
    $('.status').click(function(e){
        e.preventDefault();
        var $this = $(this);
        var msg = $this.data('msg');
        var status = $this.data('status');
        $.post(url('admin/modulos/mensagens/change_status'), {msg: msg, status: status}, function(data) {
            if (data) {
                $this.parent().parent().remove();
            }
        });
    });

    change_price = function(el) {
        if (!el.find('input').length) {
            el.html('R$ <input class="no-style" value="'+el.text().replace("R$","")+'" />');
            el.find('input').focus();
            $(el.find('input')).focusout(function(){
                text = $(this).val().replace(",",".");
                $(this).remove();
                $.ajax({
                  type: 'POST',
                  dataType: 'json',
                  url: url('admin/modulos/estoques/promo'),
                  data: {id:el.parent().data('id'), value: text},
                  success: function (data) {
                     console.log((data.value + '').replace(".",","));
                     el.text('R$ ' + (data.value + '').replace(".",","));
                  }
                });
            });
        }
    };
    $('.promo_price').click(function() {change_price($(this))});
});

$(document).ready(function(){
    
    $('.password').parent().hide();
    
    $("input#edit_password").click(function(){ 
        if($(this).is(":checked")){ 
            $('.password').parent().show();
        }else{ 
            $('.password').parent().hide();
        } 
    });
});