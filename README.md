# [CMS](http://ezoom.com.br) - Painel de Gerenciamento

Este é o painel de gerenciamento utilizado para adicionar conteúdo ao website.

## Características

- Framework Laravel
- Autenticação com Bundle Sentry
- CMS separado do website
- [Bundle Cielo](https://bitbucket.org/ezoom/cms-bundles/src)
- [Bundle PagSeguro](https://bitbucket.org/ezoom/cms-bundles/src)
- Cadastro de Modulos
- Instalação/Criação das tabelas automaticamente.

## Instalação

Altere as informações do banco de dados no arquivo application/config/database.php ou application/config/local/database.php
```
'mysql' => array(
	'driver'   => 'mysql',
	'host'     => 'localhost',
	'database' => 'database',
	'username' => 'root',
	'password' => '',
	'charset'  => 'utf8',
	'prefix'   => '',
),
```

Altera as permissões das pastas storage e bundles/basset/compiled caso necessário
```
chmod -R 777 storage
```

Cria as tabelas necessárias para funcionamento do CMS.
O Usuário padrão é ezoom@ezoom.com.br senha ezoom
```
php artisan admin::install:all
```